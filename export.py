# coding=utf-8
# Copyright @akikaaa.

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from xlnet.run_classifier import *
from xlnet import model_utils
from xlnet.prepro_utils import preprocess_text, encode_ids

FLAGS = flags.FLAGS
FLAGS(sys.argv)


def serving_input_receiver_fn():
    input_ids = tf.placeholder(dtype=tf.int64, shape=[None, FLAGS.max_seq_length], name='input_ids')
    input_mask = tf.placeholder(dtype=tf.float32, shape=[None, FLAGS.max_seq_length], name='input_mask')
    segment_ids = tf.placeholder(dtype=tf.int32, shape=[None, FLAGS.max_seq_length], name='segment_ids')
    label_ids = tf.placeholder(dtype=tf.int64, shape=[None, ], name='label_ids')
    is_real_example = tf.placeholder(dtype=tf.int64, shape=[None, ], name='is_real_example')



    receive_tensors = {'input_ids': input_ids, 'input_mask': input_mask, 'segment_ids': segment_ids,
                       'label_ids': label_ids, 'is_real_example': is_real_example}
    features = {'input_ids': input_ids, 'input_mask': input_mask, 'segment_ids': segment_ids, "label_ids": label_ids, 'is_real_example': is_real_example}
    return tf.estimator.export.ServingInputReceiver(features, receive_tensors)

def main(_):
    tf.logging.set_verbosity(tf.logging.INFO)

    processors = {
        "event-type": EventTypeProcessor,
    }

    task_name = FLAGS.task_name.lower()

    if task_name not in processors:
        raise ValueError("Task not found: %s" % task_name)

    processor = processors[task_name]()

    label_list = processor.get_labels("xlnet_labels_list.data")

    run_config = model_utils.configure_tpu(FLAGS)

    num_train_steps = None
    num_warmup_steps = None

    sp = spm.SentencePieceProcessor()
    sp.Load(FLAGS.spiece_model_file)
    def tokenize_fn(text):
        text = preprocess_text(text, lower=FLAGS.uncased)
        return encode_ids(sp, text)

    model_fn = get_model_fn(len(label_list) if label_list is not None else None)
    estimator = tf.estimator.Estimator(
        model_fn=model_fn,
        config=run_config)
    estimator.export_savedmodel(FLAGS.serving_model_save_path, serving_input_receiver_fn)


if __name__ == "__main__":
    tf.app.run()
